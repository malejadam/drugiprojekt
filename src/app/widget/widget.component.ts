import { Component, OnInit } from "@angular/core"
import { CallService } from "../call.service"

@Component({
  selector: "app-widget",
  templateUrl: "./widget.component.html",
  styleUrls: ["./widget.component.css"],
})

export class WidgetComponent implements OnInit {
  number: string = ""
  user: string = Math.random().toString(36).substring(7)
  validator = /(^[0-9]{9}$)/
  state: string = "waiting"
  interval: number
  messages: {}
  mess: string = ""

  ngOnInit() {
    this.getMessage()
  }
  constructor(private callService: CallService) { }

  call() {
    if (this.isValidNumber()) {
      this.callService.placeCall(this.number)
      this.callService.getCallId().subscribe(id => {
        this.checkStatus()
      })
    } else {
      console.info("Numer niepoprawny")
    }
  }

  sendMessage(){
    if(this.isInvalidMessage())
    {
      let message = {
        message: this.mess,
        userId: this.user
      }
      this.callService.sendMessage(message);
      this.getMessage();
    }
    else
    {
      console.info('Bład przy wysyłaniu');
    }
  }

  isValidNumber(): Boolean {
    return this.validator.test(this.number)
  }

  isInvalidMessage(): Boolean {
    if(this.mess.length > 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  getMessage(){
    this.messages = this.callService.getMessage();
  }

  checkStatus() {
    this.callService.getCallStatus().subscribe(state => {
      this.state = state
    })
  }
}
