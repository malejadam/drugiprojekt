#Instrukcja uruchomienia aplikajci:
1. Pobierz repozytorium:
  1.1 Wejdź w link https://bitbucket.org/malejadam/drugiprojekt/src/master/
  1.2 W prawym górnym rogu widnieje przycisk z napisem "clone", kliknij go
  1.3 Skopiuj zaznaczony na niebiesko link
  1.4 Na komputerze uruchom Git Bash (lub inny terminal)
  1.5 Wklej przy pomocy kombinacji przycisków Shift+Insert skopiowany link
  1.6 Uruchom pobrany folder w Visual Studio Code

2. Uruchamianie aplikacji
  2.1 W Visual Studio Code uruchom terminal
  2.2 Wpisz polecenie 'ansible-playbook playbook.yml -i inventory --connection=local -K'
  2.3 Po zainstalowaniu wszystkich potrzebnych paczek w terminalu wpisz 'ng serve'
  2.4 Po wywietleniu komunikatu 'Compiled successfully', uruchom przeglądarkę,
      a następnie w adresie URL wprowadź localhost:4200

3. Uruchomienie serwera aplikacji
  3.1 Jako serwer posłużę się serwerem, który stworzyłem na pierwszym projekcie
  3.2 W przeglądarce uruchom link https://bitbucket.org/malejadam/pierwszyprojekt/src/master/
  3.3 Następnie postępuj zgodnie z punktami od 1.2 do 1.6
  3.4 Następnie w pliku package.json w sekcji dependencies dodaj:
      "dialer": "https://bitbucket.org/pdombrowski/dialer.git"
      a następnie w terminalu wpisz 'npm install' i nacisnij enter
  3.5 Kolejnym krokiem jest wpisanie 'npm install cors', kliknij enter
  3.6 Oraz pobranie 'npm install socket.io', klikamy enter
  3.7 W pliku dialer.js w linijce 12 znajduje się pole, w którym trzeba wpisać swój login, jest to pole WYMAGANE
  3.8 W pliku dialer.js w linijce 13 znajduje się pole, w którym trzeba wpisać swóje hasło, jest to pole WYMAGANE
  3.9 Aby uruchomić serwer wpisz 'node dialer.js' oraz uruchom

4. Aby wykonać połączenie przechodzimy do localhost:4200 oraz w polu zaznaczonym 
   na czerwono wpisujemy swój numer telefonu, musi on być poprawny inaczej połączenie się nie wykona

   
